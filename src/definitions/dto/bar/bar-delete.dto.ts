import {IsString, MinLength} from "class-validator";
import {Expose, Transform} from "class-transformer";

export class BarDeleteDTO {
    @MinLength(1)
    @IsString()
    @Expose()
    id: string;
}