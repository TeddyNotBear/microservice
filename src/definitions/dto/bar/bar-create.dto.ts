import {IsDate, IsOptional, IsString, MaxDate, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";
import {ICocktail} from "../../models";

export class BarCreateDTO {

    @MinLength(1)
    @IsString()
    @Expose()
    location: string;

    @MinLength(1)
    @IsString()
    @Expose()
    addresse: string;

    @IsString()
    @Expose()
    cocktails: Array<ICocktail>;

}