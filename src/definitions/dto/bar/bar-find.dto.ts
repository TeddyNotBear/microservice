import {IsString, MinLength} from "class-validator";
import {Expose, Transform} from "class-transformer";

export class BarFindDTO {
    @MinLength(1)
    @IsString()
    @Expose()
    id: string;
}