import {IsDate, IsOptional, IsString, MaxDate, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";
import {ICocktail} from "../../models";

export class BarUpdateDTO {

    @MinLength(1)
    @IsString()
    location: string;

    @MinLength(1)
    @IsString()
    addresse: string;

    @IsString()
    cocktails: Array<ICocktail>;

}