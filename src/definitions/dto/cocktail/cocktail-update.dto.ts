import {IsDate, IsOptional, IsString, MaxDate, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";

export class CocktailUpdateDTO {

    @IsString()
    nom: string;

    @IsString()
    alcool: string;

    @IsString()
    ingredients: string;

    @IsString()
    description: string;
}