import {IsString, MinLength} from "class-validator";
import {Expose, Transform} from "class-transformer";

export class CocktailFindDTO {
    @MinLength(1)
    @IsString()
    @Expose()
    nom: string;
}