import {IsDate, IsOptional, IsString, MaxDate, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";

export class CocktailCreateDTO {

    @MinLength(1)
    @IsString()
    @Expose()
    nom: string;

    @MinLength(1)
    @IsString()
    @Expose()
    alcool: string;

    @MinLength(1)
    @IsString()
    @Expose()
    ingredients: string;

    @MinLength(1)
    @IsString()
    @Expose()
    description: string;
}