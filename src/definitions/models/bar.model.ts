import { ICocktail } from "./cocktail.model";

export interface IBar{
    _id: any; // ObjectID
    location: [string, string]; // lat, lon
    addresse: string;
    cocktails: Array<ICocktail>;
    createdDate: Date;
    updatedDate: Date;
}