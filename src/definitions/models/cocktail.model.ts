export interface ICocktail{
    _id: any; // ObjectID
    nom: string;
    alcool: string;
    ingredients: string;
    description: string;
    createdDate: Date;
    updatedDate: Date;
}