import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {BarSchema, IBarDocument, ICocktailDocument} from "../schemas";

import {BarCreateDTO, BarDeleteDTO, BarFindDTO, BarUpdateDTO, CocktailDeleteDTO} from "../../definitions";

export class BarService extends BaseService{
    protected model: Model<IBarDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<IBarDocument>('Bar',BarSchema);
    }

    public create(dto: BarCreateDTO):Promise<IBarDocument>{
        return this.model.create(dto)
    }

    public getWithId(dto : BarFindDTO): Promise<IBarDocument | null> {
        return this.model.findById(dto).exec();
    }

    public deleteWithNom(dto : BarDeleteDTO): Promise<any> {
        return this.model.deleteOne(dto).exec();
    }

    public updateWithNom(dto : BarUpdateDTO): Promise<any> {
        return this.model.updateOne(dto).exec();
    }
}