import {Mongoose} from "mongoose";

export abstract class BaseService{
    protected connection: Mongoose;

    protected constructor(connection: Mongoose){
        this.connection = connection;
    }
}