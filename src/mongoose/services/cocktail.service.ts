import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {CocktailSchema, ICocktailDocument} from "../schemas";

import {CocktailCreateDTO, CocktailFindDTO, CocktailDeleteDTO} from "../../definitions";

export class CocktailService extends BaseService{
    protected model: Model<ICocktailDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<ICocktailDocument>('Cocktail',CocktailSchema);
    }

    public create(dto: CocktailCreateDTO):Promise<ICocktailDocument>{
        return this.model.create(dto)
    }

    public getWithNom(dto : CocktailFindDTO): Promise<ICocktailDocument | null> {
        return this.model.findOne(dto).exec();
    }

    public deleteWithNom(dto : CocktailDeleteDTO): Promise<any> {
        return this.model.deleteOne(dto).exec();
    }

    public updateWithNom(dto : CocktailDeleteDTO): Promise<any> {
        return this.model.updateOne(dto).exec();
    }

}