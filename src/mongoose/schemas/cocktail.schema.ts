import {Schema, SchemaTypes} from "mongoose";
import {ICocktail} from "../../definitions";

export const CocktailSchema = new Schema({
    nom: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1,
        unique: true
    },
    alcool: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1,
    },
    ingredients: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    },
    description: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    },
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'accounts'
});

export type ICocktailDocument = ICocktail & Document;