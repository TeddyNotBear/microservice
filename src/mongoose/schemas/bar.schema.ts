import {Schema, SchemaTypes} from "mongoose";
import {IBar} from "../../definitions";

export const BarSchema = new Schema({
    location: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1,
    },
    addresse: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1,
    },
    cocktails: {
        type: SchemaTypes.Array,
        required: true,
    },
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'accounts'
});

export type IBarDocument = IBar & Document;