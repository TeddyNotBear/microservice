import {BaseController} from "./base.controller";
import {Router, Request, Response} from "express";
import {MongooseUtils} from "../../mongoose";
import {CocktailCreateDTO, CocktailDeleteDTO, CocktailUpdateDTO, ICocktail} from "../../definitions";
import {plainToInstance} from "class-transformer";

declare module 'express' {
    export interface Request {
        cocktail?: ICocktail;
    }
}

export class CocktailController extends BaseController {

    async subscribe(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailCreateDTO, req.body,res);
        if(!dto) return;

        try{
            const cocktail = await this.serviceRegistry.cocktailService.create(dto);
            res.status(201).json(cocktail);
        }catch(err){
            if(MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end();
            }

            res.status(500).end();
        }
    }

    async findByNom(req: Request, res: Response){
        res.json(req.cocktail);
    }

    async deleteByNom(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailDeleteDTO, req.body,res);
        if(!dto) return;

        const cocktail = await this.serviceRegistry.cocktailService.deleteWithNom(dto);
        if(!cocktail) {
            res.status(402).end();
            return;
        }

        res.status(201).json(cocktail);
    }

    async updateByNom(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailUpdateDTO, req.body,res);
        if(!dto) return;

        const cocktail = await this.serviceRegistry.cocktailService.updateWithNom(dto);
        if(!cocktail) {
            res.status(402).end();
            return;
        }

        res.status(201).json(cocktail);
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('/subscribe',this.subscribe.bind(this));
        router.get('/findByNom',this.findByNom.bind(this));
        router.delete('/deleteByNom',this.deleteByNom.bind(this));
        router.put('/updateByNom',this.updateByNom.bind(this));
        return router;
    }
}