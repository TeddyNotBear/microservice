import {BaseController} from "./base.controller";
import {Router, Request, Response} from "express";
import {MongooseUtils} from "../../mongoose";
import {
    BarCreateDTO,
    BarDeleteDTO,
    BarFindDTO,
    BarUpdateDTO,
} from "../../definitions";

export class BarController extends BaseController {

    async subscribe(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarCreateDTO, req.body,res);
        if(!dto) return;

        try{
            const bar = await this.serviceRegistry.barService.create(dto);
            res.status(201).json(bar);
        }catch(err){
            if(MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end();
            }

            res.status(500).end();
        }
    }

    async findById(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarFindDTO, req.header("id"),res);
        if(!dto) return;

        const bar = await this.serviceRegistry.barService.getWithId(dto);
        if(!bar) {
            res.status(402).end();
            return;
        }

        res.status(201).json(bar);
    }

    async updateByNom(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarUpdateDTO, req.body,res);
        if(!dto) return;

        const cocktail = await this.serviceRegistry.barService.updateWithNom(dto);
        if(!cocktail) {
            res.status(402).end();
            return;
        }

        res.status(201).json(cocktail);
    }

    async deleteByNom(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarDeleteDTO, req.body,res);
        if(!dto) return;

        const bar = await this.serviceRegistry.barService.deleteWithNom(dto);
        if(!bar) {
            res.status(402).end();
            return;
        }

        res.status(201).json(bar);
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('/subscribe',this.subscribe.bind(this));
        router.get('/findById',this.findById.bind(this));
        router.delete('/deleteByNom',this.deleteByNom.bind(this));
        router.delete('/updateByNom',this.updateByNom.bind(this));
        return router;
    }
}