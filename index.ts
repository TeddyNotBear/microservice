import 'reflect-metadata';
import {config} from "dotenv";
import {MongooseUtils, ICocktailDocument, CocktailSchema, ServiceRegistry} from "./src/mongoose";
import { startServer } from "./src/express";
config();


async function main(){
    const connection = await MongooseUtils.connect();
    const serviceRegistry = new ServiceRegistry(connection);
    startServer(serviceRegistry);
}

main().catch(console.error);