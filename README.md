Gitlab link : https://gitlab.com/TeddyNotBear/microservice

Cocktail routes :
- Create : localhost:3000/cocktail/subscribe
- Request : localhost:3000/cocktail/findByNom
- Update : localhost:3000/cocktail/updateByNom
- Delete : localhost:3000/cocktail/deleteByNom

Bar routes :
- Create : localhost:3000/bar/subscribe
- Request : localhost:3000/bar/findById
- Update : localhost:3000/bar/updateByNom
- Delete : localhost:3000/bar/deleteByNom